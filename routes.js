const { promisify } = require('util');
const fs = require('fs');
const http = require('http');

const rf = promisify(fs.readFile);
const get = promisify(http.get)

module.exports = {
  GET: {
    '/': async (req, res) => {
      const indexFile = await rf('./views/index.html');

      res.writeHead(200, {
          'Content-Type': 'text/html'
        });
      res.end(indexFile);
    }
  },
  POST: {
    '/auth': async (req, res) => {
      let body = '';

      req.on('data', (chunk) => {
        body += chunk.toString();
      });

      req.on('end', () => {
        const json = JSON.parse(body);
        const { pass } = json;

        if (!pass || (pass !== process.env.SECRET)) {
          res.writeHead(403, {
            'Content-Type': 'application/json'
          });

          return res.end(JSON.stringify({error: 'Not authorized'}));
        }

        http.get('http://[::1]:8081', (cameraStream) => {
          cameraStream.pipe(res);
        });
      });
    }
  }
};