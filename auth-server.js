require('./config');

const http = require('http');
const server = http.createServer();
const routes = require('./routes');

server.on('request', (req, res) => {
  const handler = routes[req.method][req.url];

  if (handler) return handler(req, res);

  res.writeHead(404);
  res.end();
});

server.listen(1234, () => console.log('Server running...'));